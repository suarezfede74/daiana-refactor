import { getToken } from "./localStorageManager"

export const URL = "https://daianait.com"
export const devRoute = "/dev"
export const getConfig = (configMethod = "GET", configBody) => {
    let config = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + getToken()
        },
        method: configMethod,
        body: configBody
    }

    if (configBody) config.body = configBody

    return config
}