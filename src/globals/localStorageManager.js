export const setToken = (value) => {
    localStorage.setItem("daianaToken", value)
}

export const getToken = () => {
    return localStorage.getItem("daianaToken")
}

export const deleteToken = () => {
    localStorage.removeItem("daianaToken")
}