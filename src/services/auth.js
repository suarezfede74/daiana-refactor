import { deleteToken, getToken } from '../globals/localStorageManager'
import { routeToLogin } from '../globals/router'
import { URL, getConfig } from '../globals/constants'

export const login = async (user, pass, isLoading) => {
    isLoading(true)
    const config = getConfig("POST", `grant_type=&username=${user}&password=${pass}&scope=&client_id=&client_secret=`)
    const response = await fetch(`${URL}/app/v1/login/token`, config);

    if(response.ok) {
        return {
            ok: true,
            data: await response.json()
        }
    }
    else {
        return {
            ok: false,
            data: await response.json()
        }
    }
}

export const logout = () => {
    if(getToken()){
        deleteToken()
        routeToLogin()
    }
}