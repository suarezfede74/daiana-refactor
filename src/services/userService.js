import { URL, getConfig } from '../globals/constants'

export const getUserData = async () => {
    let config = getConfig()
    const response = await fetch(`${URL}/app/v1/users/me`, config)
    console.log(response)
    if(response.ok){
        return {
            ok: true,
            data: await response.json()
        }
    }
    else{
        return {
            ok: false,
            data: await response.json()
        }
    }
}