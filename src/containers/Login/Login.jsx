import React, { useState } from 'react'
import GenericInput from '../../components/inputs/GenericInput'
import Button from '../../components/buttons/Button'
import { login } from '../../services/auth'
import { setToken } from '../../globals/localStorageManager'
import Logo from '../../assets/logotipo/transparente/logo_fondo_claro.png'
import User from '../../assets/icons/user.svg'
import Lock from '../../assets/icons/lock.svg'
import './login.css'
import { routeToDashboard } from '../../globals/router'

const Login = () => {
    const [user, setUser] = useState('')
    const [pass, setPass] = useState('')
    const [isLoading, setLoading] = useState(false)

    const handleLogin = async () => {
        let res = await login(user, pass, setLoading)
        if(res.ok) {
            setToken(res.data.access_token)
            routeToDashboard()
        }
    }

    return (
        <div className='login'>
            <div className='form shadow'>
                <img alt="daiana" src={Logo} />
                <div className='inputsContainer'>
                    <div className='loginInput'>
                        <GenericInput
                            type="text"
                            icon={User}
                            placeholder="Usuario"
                            value={user}
                            setValue={setUser}
                            />
                    </div>
                    <div className='loginInput'>
                        <GenericInput
                            type="password"
                            icon={Lock}
                            placeholder="Contraseña"
                            value={pass}
                            setValue={setPass}
                            />
                    </div>
                </div>
                <div className='loginBtn'>
                    <Button text={isLoading ? "CARGANDO" : "LOGIN"} disabled={user === "" || pass === ""} onClick={handleLogin} />
                </div>
            </div>
        </div>
    )
}

export default Login
