import React, {useState} from 'react'
import SmallLogo from '../../assets/isotipo/transparente/isotipo_fondo_oscuro.png'
import './sidebar.css'

const Sidebar = (props) => {
    const [open, setOpen] = useState(window.innerHeight < window.innerWidth)

    return (
        <div className={'sidebar ' + ( open ? 'open shadow' : 'closed')}>
            <div className='sideMenu'>
                <div className='userBox'>
                    <img src={props.user.avatar || SmallLogo} alt="daiana logo" />
                    <div className='userNick'>
                        <h2>Hola!</h2>
                        <p>{props.user.username}</p>
                    </div>
                </div>
            </div>
            <div className='openArrow' onClick={() => setOpen(!open)}>{open ? '<' : '>'}</div>
            <div className='sidebarImg'>
                <img alt="daiana logo" />
            </div>
            
        </div>
    )
}

export default Sidebar
