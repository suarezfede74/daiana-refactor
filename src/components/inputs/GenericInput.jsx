import React from 'react'
import './inputs.css'

const GenericInput = (props) => {
    return (
        <div className='genericInput'>
            {props.icon && <img alt="input icon" src={props.icon} />}
            <input 
                type={props.type} 
                value={props.value} 
                onChange={(e) => props.setValue(e.target.value)}
                placeholder={props.placeholder} />
        </div>
    )
}

export default GenericInput
