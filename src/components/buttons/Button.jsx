import React from 'react'
import './buttons.css'

const Button = (props) => {
    const handleClick = () => {
        if (!props.disabled){
            props.onClick()
        }
    }

    return (
        <div className={'button ' + (props.disabled ? 'disabled' : '')} onClick={handleClick}>
            {props.text}
        </div>
    )
}

export default Button
