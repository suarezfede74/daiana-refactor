import React, {useEffect, useState} from 'react'
import Login from './containers/Login/Login';
import { getToken } from './globals/localStorageManager'
import Dashboard from './containers/Dashboard/Dashboard'
import Measurements from './containers/Measurements/Measurements'
import Plants from './containers/Plants/Plants'
import Sidebar from './containers/SideBar/Sidebar';
import Navbar from './containers/Navbar/Navbar';
import { getUserData } from './services/userService';
import { Context } from './storage/context';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import { logout } from './services/auth';

const App = () => {
  const [user, setUser] = useState({})

  useEffect(() => {
    const getUData = async () => {
      let res = await getUserData()
      if (res.ok) setUser(res.data)
      else logout()
    }
    getUData()
  }, [])

  if (!getToken()){
    return <Login />
  }

  let contextValues = {
    user: user,
    setUser: setUser
  }

  return (
    <Context.Provider value={contextValues}>
      <div className='app'>
        <Router>
          <Sidebar user={user} />
          <Navbar />
          <Routes>
            <Route exact path="/Dashboard" element={<Dashboard />} />
            <Route exact path="/Measurements" element={<Measurements />} />
            <Route exact path="/Plants" element={<Plants />} />
            <Route path="/" element={<Navigate from="*" to="/Dashboard" />} />
          </Routes>
        </Router>
      </div>
    </Context.Provider>
  );
}

export default App;
